package optional

import (
	"errors"
)

type optional struct {
	args    Args
	asserts []Assert
	err     error
}

func Of(args ...interface{}) Optional {
	return &optional{args: args, asserts: []Assert{NilAssert()}}
}

//handleError 异常处理
func (optional *optional) handleError(err interface{}) {
	if errMsg, ok := err.(string); ok {
		optional.err = errors.New(errMsg)
	} else {
		optional.err = errors.New("Unknown Error Message!")
	}
}

//actionTemplate 方法的通用行为
func (optional *optional) actionTemplate(action func()) (r Optional) {
	defer func() {
		if err := recover(); err != nil {
			optional.handleError(err)
			r = optional
		}
	}()
	action()
	return optional
}

// Map 断言成功后将params经mapper规则变换后生成新的Optional返回,mapper处理的结果以[]interface{}的形式返回
func (optional *optional) Map(mapper func(args Args) Args) (r Optional) {
	return optional.actionTemplate(func() {
		if optional.err == nil && optional.assert(optional.args) {
			optional.args = mapper(optional.args)
		}
	})
}

// Action 断言成功后进行action动作
func (optional *optional) Action(action func(args Args)) (r Optional) {
	return optional.actionTemplate(func() {
		if optional.err == nil && optional.assert(optional.args) {
			action(optional.args)
		}
	})
}

// ElseMap 断言失败后将params经mapper规则变换后生成新的Optional返回,mapper处理的结果以[]interface{}的形式返回
func (optional *optional) ElseMap(mapper func(args Args) Args) (r Optional) {
	return optional.actionTemplate(func() {
		if optional.err == nil && !optional.assert(optional.args) {
			optional.args = mapper(optional.args)
		}
	})
}

// ElseAction 断言失败后进行action动作
func (optional *optional) ElseAction(action func(args Args)) (r Optional) {
	return optional.actionTemplate(func() {
		if optional.err == nil && !optional.assert(optional.args) {
			action(optional.args)
		}
	})
}

// assert 调用断言链进行断言
func (optional *optional) assert(args Args) bool {
	if optional.asserts == nil || len(optional.asserts) <= 0 {
		return true
	}
	for i := 0; i < len(optional.asserts); i++ {
		if !optional.asserts[i](args) {
			return false
		}
	}
	return true
}

// AddAssert 在断言链后面添加一个新的断言
func (optional *optional) AddAssert(ast Assert) Optional {
	if optional.asserts != nil {
		optional.asserts = append(optional.asserts, ast)
	} else if ast != nil {
		optional.asserts = []Assert{ast}
	}
	return optional
}

// AssertChain 重新设置一个断言链
func (optional *optional) AssertChain(asserts ...Assert) Optional {
	optional.asserts = asserts
	return optional
}

// DefaultAssert 将断言链设置成默认的
func (optional *optional) DefaultAssert() Optional {
	optional.asserts = []Assert{NilAssert()}
	return optional
}

func (optional *optional) ErrorAssert() Optional {
	optional.asserts = []Assert{ErrorAssert()}
	return optional
}

// OrElse 如果断言成功，返回正确结果，否则，返回other
func (optional *optional) OrElse(other ...interface{}) (r Args) {
	defer func() {
		if err := recover(); err != nil {
			optional.handleError(err)
			r = other
		}
	}()
	if optional.assert(optional.args) {
		return optional.args
	}
	return other
}

// OrElseGet 如果断言成功，返回正确结果，否则，返回other函数的回调值
func (optional *optional) OrElseGet(other func(args Args) Args) (r Args) {
	defer func() {
		if err := recover(); err != nil {
			optional.handleError(err)
			r = optional.OrElse()
		}
	}()
	return optional.OrElse(other(optional.args))
}

// OrElseIndex 如果断言成功，返回索引位置为i的结果值，否则，返回other
func (optional *optional) OrElseIndex(i int, other interface{}) (r interface{}) {
	defer func() {
		if err := recover(); err != nil {
			optional.handleError(err)
			r = other
		}
	}()
	if i >= 0 && i < len(optional.args) && optional.assert(optional.args) {
		return optional.args[i]
	}
	return other
}

// OrElseGetIndex 如果断言成功，返回索引位置为i的结果值，否则，返回other函数的回调值
func (optional *optional) OrElseGetIndex(i int, other func(args Args) interface{}) (r interface{}) {
	defer func() {
		if err := recover(); err != nil {
			optional.handleError(err)
			r = optional.OrElseIndex(i, nil)
		}
	}()
	return optional.OrElseIndex(i, other(optional.args))
}

// Get 不进行断言，直接返回结果
func (optional *optional) Get() Args {
	return optional.args
}

//Error 返回处理过程中的异常
func (optional *optional) Error() error {
	return optional.err
}

//HandleError 对返回过程中的异常进行处理
func (optional *optional) HandleError(handler func(err error, args Args) (bool, Args)) (r Optional) {
	return optional.actionTemplate(func() {
		if optional.err != nil {
			//如果处理完成后返回true，则去除掉当前异常，后续步骤可正常进行  args用户指定的后续处理数据
			if removeError, args := handler(optional.err, optional.args); removeError {
				optional.err = nil
				if args == nil {
					args = Args{}
				}
				optional.args = args
			}
		}
	})
}
