package test

import (
	"fmt"
	"gitee.com/li3275637019/optional"
	"testing"
)

func F(params ...interface{}) (int, bool, error) {
	fmt.Println(params...)
	return len(params), true, nil
}

func TestParams(t *testing.T) {
	//optional.Of("1",errors.New("2")).Map(func(params []interface{}) interface{} {
	//	return nil
	//})
	//optional.Of(F())
	//f, err := F()
	//list := []interface{}{f,err}
	//t.Log(list)
	i := 1
	t.Log(fmt.Sprintf("%v", &i))
	t.Log(optional.Of(F(1, 2, 3, "aa")).AssertChain(optional.ErrorAssert(), optional.NilAssert(1, 0), func(args optional.Args) bool {
		return args[1].(bool)
	}).Map(func(params optional.Args) optional.Args {
		i := params[0].(int)
		i++
		t.Log(i)
		return optional.Return([]int{i, i + 1})
	}).ErrorAssert().Map(func(params optional.Args) optional.Args {
		t.Log(params)
		return optional.Return("hello optional")
	}).OrElseIndex(0, "failed optional"))
}

func TestErrorHandle(t *testing.T) {
	arr := []int{1, 2, 3, 4, 5}
	t.Log(optional.Of(arr).Map(func(args optional.Args) optional.Args {
		t.Log(args[0])
		arr := args[0].([]int)
		arr[0] = 2
		return optional.Return(arr, 1)
	}).Map(func(args optional.Args) optional.Args {
		t.Log(args[0])
		arr := args[0].([]int)
		if len(arr) < 6 {
			panic("error test!!!!!!!!")
		}
		arr[5] = 1
		return optional.Return(arr, 2)
	}).Map(func(args optional.Args) optional.Args {
		arr := args[0].([]int)
		arr[1] = 3
		return optional.Return(arr, 3)
	}).HandleError(func(err error, args optional.Args) (bool, optional.Args) {
		t.Log(err)
		switch args[1] {
		case 1:
			t.Log("异常来自第一步后")
		case 2:
			t.Log("异常来自第二步后")
		case 3:
			t.Log("异常来自第三步后")
		default:
			t.Log("未知异常")
		}
		return true, optional.Return([]int{5, 4, 3, 2, 1}, "error recover result!!!!!")
	}).OrElse([]int{}))
}

func TestAction(t *testing.T) {
	arr := []int{1, 2, 3, 4, 5}
	t.Log(optional.Of(arr).Action(func(args optional.Args) {
		arr := args[0].([]int)
		for i, e := range arr {
			t.Log("index:", i, "value:", e)
		}
	}).AssertChain(func(args optional.Args) bool {
		return false
	}).ElseAction(func(args optional.Args) {
		t.Log("断言失败后的行为")
	}).Error())
}
