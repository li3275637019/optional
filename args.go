package optional

type Args []interface{}

func Return(args ...interface{}) Args {
	return args
}
