package optional

type Optional interface {
	// Map 断言成功后将params经mapper规则变换后生成新的Optional返回,mapper处理的结果以Args的形式返回
	Map(mapper func(args Args) Args) Optional
	// Action 断言成功后进行action动作
	Action(action func(args Args)) Optional
	// ElseMap 断言失败后将params经mapper规则变换后生成新的Optional返回,mapper处理的结果以Args的形式返回
	ElseMap(mapper func(args Args) Args) Optional
	// ElseAction 断言失败后进行action动作
	ElseAction(action func(args Args)) Optional
	// AddAssert 在断言链后面添加一个新的断言
	AddAssert(ast Assert) Optional
	// AssertChain 重新设置一个断言链
	AssertChain(asserts ...Assert) Optional
	// DefaultAssert 将断言链设置成默认的
	DefaultAssert() Optional
	// ErrorAssert 将断言链设置成异常断言
	ErrorAssert() Optional
	// OrElse 如果断言成功，返回正确结果，否则，返回other
	OrElse(other ...interface{}) Args
	// OrElseGet 如果断言成功，返回正确结果，否则，返回other函数的回调值
	OrElseGet(other func(args Args) Args) Args
	// OrElseIndex 如果断言成功，返回索引位置为i的结果值，否则，返回other
	OrElseIndex(i int, other interface{}) interface{}
	// OrElseGetIndex 如果断言成功，返回索引位置为i的结果值，否则，返回other函数的回调值
	OrElseGetIndex(i int, other func(args Args) interface{}) interface{}
	// Get 不进行断言，直接返回结果
	Get() Args
	//Error 返回处理过程中的异常
	Error() error
	//HandleError 对返回过程中的异常进行处理,并设置后续的处理数据
	HandleError(handler func(err error, args Args) (bool, Args)) Optional
}
